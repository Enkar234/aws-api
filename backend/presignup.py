import json
import boto3
from botocore.exceptions import ClientError

AWS_REGION = "us-east-1"

client = boto3.client('ses',region_name=AWS_REGION)

def lambda_handler(event, context):
    email = event['request']['userAttributes']['email']
    try:
        response = client.verify_email_identity(EmailAddress=email)
    
    except ClientError as e:
        print(e.response['Error']['Message'])
    
    else:
        print('Verification email sent to ' + email + '. Request ID: ' + response['ResponseMetadata']['RequestId'])
    
    event["response"]["autoConfirmUser"] = True
    event["response"]["autoVerifyEmail"] = True
    return event
