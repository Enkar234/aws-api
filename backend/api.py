import json
from data_provider import DataProvider, LogicProvider
import jwt


def get_users_email(event):
    token = event['headers']['Authorization'].split()[1]
    email = jwt.decode(token, verify=False)['email']
    return email


dataProvider = DataProvider()
logicProvider = LogicProvider()

def create_project(event, context):
    title = event['queryStringParameters']['title']
    subject = event['queryStringParameters']['subject']
    members = json.loads(event['body'])
    logicProvider.CreateProject(title, subject, members)
    return {
        "statusCode": 200,
    }

def get_projects(event, context):
    email = get_users_email(event)
    return {
        "statusCode": 200,
        "body": json.dumps(dataProvider.GetProjects(email), default=lambda x: x.__dict__),
    }

def edit_project_members(event, context):
    project_id = event['queryStringParameters']['project_id']
    members = json.loads(event['body'])
    logicProvider.EditProjectMembers(project_id, members)
    return {
        "statusCode": 200,
    }


def delete_project(event, context):
    project_id = event['queryStringParameters']['project_id']
    logicProvider.DeleteProject(project_id)
    return {
        "statusCode": 200,
    }


def leave_project(event, context):
    project_id = event['queryStringParameters']['project_id']
    email = get_users_email(event)
    logicProvider.LeaveProject(project_id, email)
    return {
        "statusCode": 200,
    }

def get_hours(event, context):
    email = get_users_email(event)
    return {
        "statusCode": 200,
        "body": json.dumps(dataProvider.GetUserFreeHours(email), default=lambda x: x.__dict__),
    }

def declare_hours(event, context):
    email = get_users_email(event)
    hours = json.loads(event['body'])
    logicProvider.DeclareHours(email, hours)
    return {
        "statusCode": 200,
    }

def get_appointments(event, context):
    email = get_users_email(event)
    return {
        "statusCode": 200,
        "body": json.dumps(dataProvider.GetAppointments(email), default=lambda x: x.__dict__),
    }

def create_appointment(event, context):
    project_id = event['queryStringParameters']['project_id']
    return {
        "statusCode": 200,
        "body": json.dumps(logicProvider.CreateAppointment(project_id), default=lambda x: x.__dict__),
    }

def delete_appointment(event, context):
    appointment_id = event['queryStringParameters']['appointment_id']
    logicProvider.DeleteAppointment(appointment_id)
    return {
        "statusCode": 200,
    }

def return_context(event, context):
    return {
        "statusCode": 200,
        "body": json.dumps(context, default=lambda x: x.__dict__),
    }