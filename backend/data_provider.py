from dto.member_dto import MemberDTO
from dto.project_dto import ProjectDTO
from dto.appointment_dto import AppointmentDTO
from dto.user_free_hours_dto import UserFreeHoursDTO
import sys
import logging
import pymysql
import os
import boto3
import uuid
import json
from datetime import datetime
#rds settings
rds_host  = "smpblyc9v40h19.cwoplhpalzqx.us-east-1.rds.amazonaws.com"
name = 'admin123'
password = 'admin123'
db_name = 'AWS'

def GetDatabaseConnection():
    conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5, cursorclass=pymysql.cursors.DictCursor)
    return conn


class DataProvider:
    def GetProjects(self, email):
        conn = GetDatabaseConnection()
        with conn.cursor() as cursorObject:
            cursorObject.execute("call GetProjectsForUser(%s)", email)
            projects = []
            for project in cursorObject.fetchall():
                appointments = []
                cursorObject.execute("call GetAppointmentsForProject(%s)", project['Id'])
                for appointment in cursorObject.fetchall():
                    appointments.append(AppointmentDTO(appointment['Id'], appointment['StartTime'], appointment['EndTime'], project['Title']))
                cursorObject.execute("call GetMembersForProject(%s)", project["Id"])
                members = []
                leader_id = ''
                for member in cursorObject.fetchall():
                    if member['IsLeader'] == 1:
                        leader_id = member['Email']
                    members.append(MemberDTO(member['Email'], member['IsLeader']))
                projects.append(ProjectDTO(project['Id'], project['Title'], project['Subject_'], leader_id, members, appointments))
        return projects
    
    def GetAppointments(self, email):
        conn = GetDatabaseConnection()
        with conn.cursor() as cursorObject:
            cursorObject.execute("call GetAppointmentsForUser(%s)", email)
            appointments = []
            for appointment in cursorObject.fetchall():
                appointments.append(AppointmentDTO(appointment['Id'], appointment['StartTime'], appointment['EndTime'], appointment['Title']))
        return appointments

    def GetUserFreeHours(self, email):
        conn = GetDatabaseConnection()
        with conn.cursor() as cursorObject:
            cursorObject.execute("call GetDeclaredHoursForUser(%s)", email)
            hours = []
            for hour in cursorObject.fetchall():
                hours.append(UserFreeHoursDTO(hour['StartTime'], hour['EndTime']))
        return hours
    

class LogicProvider:
    def CreateProject(self, title, subject, members):
        conn = GetDatabaseConnection()
        try:
            with conn.cursor() as cursorObject:
                cursorObject.execute("call CreateProject(%s, %s)", (title, subject))
                projectId = cursorObject.fetchone()['LAST_INSERT_ID()']
                for member in members:
                    cursorObject.execute("call CreateProjectMember(%s, %s, %s)", (projectId, member['email'], 1 if member['is_leader'] else 0))            
            conn.commit()
        finally:
            conn.close()
        return

    def EditProjectMembers(self, projectId, members):
        conn = GetDatabaseConnection()
        try:
            with conn.cursor() as cursorObject:
                cursorObject.execute("call RemoveAllProjectMembers(%s)", projectId)
                for member in members:
                    cursorObject.execute("call CreateProjectMember(%s, %s, %s)", (projectId, member['email'], 1 if member['is_leader'] else 0))            
            conn.commit()
        finally:
            conn.close()
        return
    
    def DeleteProject(self, projectId):
        conn = GetDatabaseConnection()
        try:
            with conn.cursor() as cursorObject:
                cursorObject.execute("call DeleteProject(%s)", projectId)           
            conn.commit()
        finally:
            conn.close()
        return
    
    def LeaveProject(self, projectId, email):
        conn = GetDatabaseConnection()
        try:
            with conn.cursor() as cursorObject:
                cursorObject.execute("SELECT COUNT(*) AS c FROM Members WHERE ProjectId=%s", projectId)
                members_count = cursorObject.fetchone()['c']
                if members_count <= 1:
                    cursorObject.execute("call DeleteProject(%s)", projectId)
                else:
                    cursorObject.execute("call RemoveProjectMember(%s, %s)", (projectId, email))
                    cursorObject.execute("SELECT COUNT(*) AS c FROM Members WHERE ProjectId=%s AND IsLeader=1", projectId)
                    leaders_count = cursorObject.fetchone()['c']
                    if leaders_count < 1:
                        cursorObject.execute("call SetProjectLeader(%s)", (projectId))
            conn.commit()
        finally:
            conn.close()
        return
    
    def DeclareHours(self, email, hours):
        conn = GetDatabaseConnection()
        try:
            with conn.cursor() as cursorObject:
                cursorObject.execute("call RemoveAllDeclaredHoursForUser(%s)", email)
                for hour in hours:
                    cursorObject.execute("call SetDeclaredHoursForUser(%s, %s, %s)", (email, hour['start'], hour['end']))
            conn.commit()
        finally:
            conn.close()
        return
    
    def DeleteAppointment(self, appointmentId):
        conn = GetDatabaseConnection()
        try:
            with conn.cursor() as cursorObject:
                cursorObject.execute("call DeleteAppointment(%s)", appointmentId)
            conn.commit()
        finally:
            conn.close()
        return


    def GetMeetingTime(self, start, end, members_hours_to_choose, appointments):
        members_hours_to_choose_copy = members_hours_to_choose.copy()
        for appointment in appointments:
            if appointment[0] <= start and appointment[1] >= end:
                return start, end, False
        if(len(members_hours_to_choose_copy) == 0):
            if(end - start > 15000):
                return start, end, True
            return start, end, False
        member = members_hours_to_choose_copy.pop()
        for hours in member:
            is_planned = False
            if hours[0] <= start and hours[1] >= end:
                result_start, result_end, is_planned = self.GetMeetingTime(start, end, members_hours_to_choose_copy, appointments)
            elif hours[0] >= start and hours[0] <= end and hours[1] >= start and hours[1] <= end:
                result_start, result_end, is_planned = self.GetMeetingTime(hours[0], hours[1], members_hours_to_choose_copy, appointments)
            if is_planned:
                return result_start, result_end, is_planned
    
        return start, end, False

    def ScheduleEmail(self, project, appointment_start, members):
        state_machine_arn = os.getenv("STATE_MACHINE_ARN")
        client = boto3.client('stepfunctions')
        transactionId = str(uuid.uuid1()) 
        send_timestamp = datetime.fromtimestamp(1592670150).isoformat() + 'Z'
        
        stepfunction_input = {
            "send_timestamp": send_timestamp,
            "recipients": [member['Email'] for member in members],
            "title": project['Title'],
            "subject": project['Subject_']
        }
        
        response = client.start_execution(
            stateMachineArn=state_machine_arn,
            name=transactionId,
            input=json.dumps(stepfunction_input)
        )

    def CreateAppointment(self, projectId):
        conn = GetDatabaseConnection()
        with conn.cursor() as cursorObject:
            cursorObject.execute("call GetProject(%s)", (projectId))
            project = cursorObject.fetchone()
            cursorObject.execute("call GetAppointmentsForProject(%s)", projectId)
            appointments = []
            for appointment in cursorObject.fetchall():
                appointments.append([appointment['StartTime'], appointment['EndTime']])
        with conn.cursor() as cursorObject:
            cursorObject.execute("call GetMembersForProject(%s)", (projectId))
            members_hours = []
            leader_id = ''
            members = cursorObject.fetchall()
            for member in members:
                cursorObject.execute("call GetDeclaredHoursForUser(%s)", member['Email'])
                hours = []
                for hour in cursorObject.fetchall():
                    hours.append([hour['StartTime'], hour['EndTime']])
                members_hours.append(hours)
            if len(members) != len(members_hours):
                return AppointmentDTO(-1, 0, 0, '')

            for hours in members_hours[0]:
                result_start, result_end, is_planned = self.GetMeetingTime(hours[0], hours[1], members_hours[1:], appointments)
                
                if is_planned:
                    try:
                        with conn.cursor() as cursorObject:
                            cursorObject.execute("call CreateAppointment(%s, %s, %s)", (projectId, result_start, result_end))
                            appointmentId = cursorObject.fetchone()['LAST_INSERT_ID()']
                        conn.commit()
                        self.ScheduleEmail(project, result_start, members)
                        return AppointmentDTO(appointmentId, result_start, result_end, project['Title'])
                    finally:
                        conn.close()
        return AppointmentDTO(-1, 0, 0, '')
