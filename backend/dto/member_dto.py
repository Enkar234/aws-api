class MemberDTO:
    def __init__(self, email, is_leader):
        self.email = email
        if is_leader == 1 or is_leader == True:
            self.is_leader = True
        else:
            self.is_leader = False
