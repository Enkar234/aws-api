class AppointmentDTO:
    def __init__(self, _id, start, end, project_title):
        self.appointment_Id = _id
        self.start = start
        self.end = end
        self.project_title = project_title
