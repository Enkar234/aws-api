class ProjectDTO:
    def __init__(self, project_Id, title, subject, leader_Id, members, appointments):
        self.project_Id = project_Id
        self.title = title
        self.subject = subject
        self.leader_Id = leader_Id
        self.members = members
        self.appointments = appointments
