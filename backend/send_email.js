var aws = require('aws-sdk');
var ses = new aws.SES({region: 'us-east-1'});

exports.handler = (event, context, callback) => {
     var params = {
        Destination: {
            ToAddresses: event.recipients
        },
        Message: {
            Body: {
                Text: { Data: 'Your AWS "' + event.project_title + '" meeting starts in 6 hours'
                    
                }
                
            },
            
            Subject: { Data: "[AWS] " + event.project_subject + " meeting"
                
            }
        },
        Source: "zmarcin27@gmail.com"
    };

    
     ses.sendEmail(params, function (err, data) {
        callback(null, {err: err, data: data});
        if (err) {
            console.log(err);
            context.fail(err);
        } else {
            
            console.log(data);
            context.succeed(event);
        }
    });
};