DELIMITER ;

DROP TABLE IF EXISTS Members;
DROP TABLE IF EXISTS Appointments;
DROP TABLE IF EXISTS DeclaredHours;
DROP TABLE IF EXISTS Projects;

Create table Projects (
    Id  int NOT NULL AUTO_INCREMENT,
    Title varchar(255) NOT NULL,
    Subject_ varchar(255),
    PRIMARY KEY (Id)
    );

Create table Members (
    Email varchar(255) NOT NULL,
    ProjectId int NOT NULL,
    IsLeader boolean NOT NULL,
    PRIMARY KEY (Email, ProjectId),
    FOREIGN KEY (ProjectId) REFERENCES Projects(Id)
    );

Create table Appointments (
    Id int NOT NULL AUTO_INCREMENT,
    StartTime bigint NOT NULL,
    EndTime bigint NOT NULL,
    ProjectId int NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (ProjectId) REFERENCES Projects(Id)
    );

Create table DeclaredHours (
    Id int NOT NULL AUTO_INCREMENT,
    Email varchar(255) NOT NULL,
    StartTime bigint NOT NULL,
    EndTime bigint NOT NULL,
    PRIMARY KEY (Id)
    );