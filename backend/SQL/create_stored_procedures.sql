DELIMITER ;

DROP PROCEDURE IF EXISTS CreateProject;
DROP PROCEDURE IF EXISTS DeleteProject;
DROP PROCEDURE IF EXISTS GetProjectsForUser;
DROP PROCEDURE IF EXISTS GetMembersForProject;
DROP PROCEDURE IF EXISTS GetAppointmentsForProject;
DROP PROCEDURE IF EXISTS CreateProjectMember;
DROP PROCEDURE IF EXISTS RemoveAllProjectMembers;
DROP PROCEDURE IF EXISTS RemoveProjectMember;
DROP PROCEDURE IF EXISTS SetProjectMemberAsLeader;
DROP PROCEDURE IF EXISTS GetDeclaredHoursForUser;
DROP PROCEDURE IF EXISTS SetDeclaredHoursForUser;
DROP PROCEDURE IF EXISTS RemoveAllDeclaredHoursForUser;
DROP PROCEDURE IF EXISTS GetAppointmentsForUser;
DROP PROCEDURE IF EXISTS DeleteAppointment;
DROP PROCEDURE IF EXISTS CreateAppointment;
DROP PROCEDURE IF EXISTS SetProjectLeader;
DROP PROCEDURE IF EXISTS GetProject;

DELIMITER #
CREATE PROCEDURE CreateProject
(
   _Title varchar(255),
   _Subject varchar(255)
) 
BEGIN 
   INSERT INTO Projects (Title, Subject_) VALUES (_Title, _Subject);
   SELECT LAST_INSERT_ID();
END #


CREATE PROCEDURE DeleteProject
(
   _Id int
) 
BEGIN
    DELETE FROM Appointments WHERE ProjectId = _Id;
    DELETE FROM Members WHERE ProjectId = _Id;
    DELETE FROM Projects WHERE Id = _Id;
END #


CREATE PROCEDURE GetProjectsForUser
(
   _email varchar(255)
) 
BEGIN 
   SELECT * FROM Projects P
   JOIN Members M ON M.ProjectId = P.Id
   WHERE M.Email = _email;
END #

CREATE PROCEDURE GetProject
(
   _id int
) 
BEGIN 
   SELECT * FROM Projects P
   WHERE P.Id = _id;
END #


CREATE PROCEDURE GetMembersForProject
(
   _projectId int
) 
BEGIN 
   SELECT * FROM Members
   WHERE ProjectId = _projectId;
END #


CREATE PROCEDURE GetAppointmentsForProject
(
   _projectId int
) 
BEGIN 
   SELECT * FROM Appointments
   WHERE ProjectId = _projectId;
END #


CREATE PROCEDURE CreateProjectMember
(
   _projectId int,
   _email varchar(255),
   _isLeader boolean
) 
BEGIN 
   INSERT INTO Members (ProjectId, Email, IsLeader) VALUES (_projectId, _email, _isLeader);
END #

CREATE PROCEDURE RemoveAllProjectMembers
(
   _projectId int
) 
BEGIN 
   DELETE FROM Members WHERE ProjectId = _projectId;
END #

CREATE PROCEDURE RemoveProjectMember
(
   _projectId int,
   _email varchar(255)
) 
BEGIN 
   DELETE FROM Members WHERE ProjectId = _projectId AND Email = _email;
END #


CREATE PROCEDURE SetProjectLeader
(
   _projectId int
) 
BEGIN 
   UPDATE Members SET IsLeader = 1
   WHERE ProjectId = _projectId
   LIMIT 1;
END #


CREATE PROCEDURE GetDeclaredHoursForUser
(
   _email varchar(255)
) 
BEGIN 
   SELECT * FROM DeclaredHours WHERE Email = _email;
END #


CREATE PROCEDURE SetDeclaredHoursForUser
(
   _email varchar(255),
   _start bigint,
   _end bigint
) 
BEGIN 
   INSERT INTO DeclaredHours (Email, StartTime, EndTime) VALUES (_email, _start, _end);
END #


CREATE PROCEDURE RemoveAllDeclaredHoursForUser
(
   _email varchar(255)
) 
BEGIN 
   DELETE FROM DeclaredHours WHERE Email = _email;
END #


CREATE PROCEDURE GetAppointmentsForUser
(
   _email varchar(255)
) 
BEGIN 
   SELECT * FROM Appointments A
   JOIN Projects P on A.ProjectId = P.Id
   JOIN Members M on M.ProjectId = P.Id
   WHERE M.Email = _email;
END #


CREATE PROCEDURE DeleteAppointment
(
   _appointmentId int
) 
BEGIN 
   DELETE FROM Appointments WHERE Id = _appointmentId;
END #

CREATE PROCEDURE CreateAppointment
(
   _projectId int,
   _startTime bigint,
   _endTime bigint
) 
BEGIN 
   INSERT INTO Appointments (ProjectId, StartTime, EndTime) VALUES (_projectId, _startTime, _endTime);
   SELECT LAST_INSERT_ID();
END #

DELIMITER ;

INSERT INTO Projects (Title, Subject_) VALUES ('Bazy danych', 'Bazka');
INSERT INTO Members(Email, ProjectId, IsLeader) VALUES ('zalewskim2@student.mini.pw.edu.pl', (SELECT Id from Projects), 1);
INSERT INTO Members(Email, ProjectId, IsLeader) VALUES ('test@xd.xd', (Select Id from Projects), 0);
INSERT INTO Appointments(StartTime, EndTime, ProjectId) VALUES (1529421670000, 1529421670000, (Select Id from Projects));
INSERT INTO DeclaredHours(Email, StartTime, EndTime) VALUES('test@xd.xd', 1529421670000, 1529421670000);